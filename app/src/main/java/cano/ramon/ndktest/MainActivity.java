package cano.ramon.ndktest;

import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

// Log.v("Object address", "--> " + m_PointerToAnObject);

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-lib");
    }

    TextView tvResult;
    EditText etNumberA;
    EditText etNumberB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvResult = (TextView) findViewById(R.id.tvResult);
        etNumberA = (EditText) findViewById(R.id.etNumberA);
        etNumberB = (EditText) findViewById(R.id.etNumberB);


        long m_PointerToAnObject = getNativeObject();
        modifyNativeObject(m_PointerToAnObject);

        MyNumber javaNum = createJavaInstance(MyNumber.class);

        MyNumber[] numArray = new MyNumber[5];
        for (int i = 0; i < numArray.length; i++) {
            numArray[i] = new MyNumber();
        }
        modifyArray(numArray);
    }

    public void addNumbers(View v) {
        int number_a = Integer.parseInt(etNumberA.getText().toString());
        int number_b = Integer.parseInt(etNumberB.getText().toString());
        int result = addTwoNumbers(number_a, number_b);
        MyNumber num = new MyNumber(result);
        getObjectAndAdd(num);
        tvResult.setText(result + " + 1 = " + num.value);
        accessMethods(num);
    }

    public native int addTwoNumbers(int a, int b);
    public native int getObjectAndAdd(MyNumber number);
    public native int accessMethods(MyNumber number);
    public native long getNativeObject();
    private native void modifyNativeObject(long objPointer);
    private native MyNumber createJavaInstance(Class cls);
    private native void modifyArray(MyNumber[] arr);
}
