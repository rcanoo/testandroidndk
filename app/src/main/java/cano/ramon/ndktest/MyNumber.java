package cano.ramon.ndktest;

import android.util.Log;

public class MyNumber {
    public int value;
    public MyNumber() {
        this.value = 0;
    }
    public MyNumber(int value) {
        this.value = value;
    }
    public int getValueMinusOne() {
        return (this.value - 1);
    }
    public void sayHello() {
        Log.v("MyNumber", "Hello!!");
    }
}
