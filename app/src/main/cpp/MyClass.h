//
// Created by user1 on 28/12/2017.
//

#ifndef NDKTEST_MYCLASS_H
#define NDKTEST_MYCLASS_H


#include <jni.h>

class MyClass {
public:
    MyClass();
    int getX();
    void setX(int n);


private:
    int x;
};


#endif //NDKTEST_MYCLASS_H
