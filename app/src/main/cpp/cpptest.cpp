#include "cpptest.h"
#include <android/log.h>
// __android_log_print(ANDROID_LOG_DEBUG, "TAG", "msg");

jint Java_cano_ramon_ndktest_MainActivity_getObjectAndAdd(
        JNIEnv *env,
        jobject /* this */,
        jobject obj) {

    jclass cls = env->GetObjectClass(obj);

    jfieldID fieldId = env->GetFieldID(cls, "value", "I");

    jint intValue = env->GetIntField(obj, fieldId);

    intValue += 1;
    env->SetIntField(obj, fieldId, intValue);

    return 0;
}

jint Java_cano_ramon_ndktest_MainActivity_addTwoNumbers(
        JNIEnv *env,
        jobject /* this */,
        jint a,
        jint b) {
    return a + b;
}

void Java_cano_ramon_ndktest_MainActivity_accessMethods(JNIEnv* env, jobject thisObj, jobject obj) {
    jclass cls = env->GetObjectClass(obj);

    jmethodID methodID_a = env->GetMethodID(cls, "getValueMinusOne", "()I");
    jint result = env->CallIntMethod(obj, methodID_a);

    jmethodID methodID_b = env->GetMethodID(cls, "sayHello", "()V");
    env->CallVoidMethod(obj, methodID_b);

    return;
}

jlong Java_cano_ramon_ndktest_MainActivity_getNativeObject(JNIEnv* env, jobject thisObj) {
    // Returns the pointer to the object
    return (jlong)new MyClass();
}

void Java_cano_ramon_ndktest_MainActivity_modifyNativeObject(JNIEnv* env, jobject thisObj, jlong objAddr) {
    MyClass* obj = (MyClass*)objAddr;
    obj->setX(15);

    obj->setX(3);
    return;
}

jobject Java_cano_ramon_ndktest_MainActivity_createJavaInstance(JNIEnv* env, jobject thisObj, jclass cls) {
    jmethodID constructId = env->GetMethodID(cls, "<init>", "()V");
    jobject obj = env->NewObject(cls, constructId, 0);

    return obj;
}

void Java_cano_ramon_ndktest_MainActivity_modifyArray(JNIEnv* env, jobject thisObj, jobjectArray objArray) {
    int len = env->GetArrayLength(objArray);
    for (int i = 0; i < len; ++i) {
        jobject obj = env->GetObjectArrayElement(objArray, i);
        jclass cls = env->GetObjectClass(obj);
        jfieldID fieldId = env->GetFieldID(cls, "value", "I");
        jint intValue = env->GetIntField(obj, fieldId);
        intValue += i;
        env->SetIntField(obj, fieldId, intValue);
    }
    return;
}
