#include <jni.h>
#include "MyClass.h"
#include <vector>

#ifndef NDKTEST_CPPTEST_H
#define NDKTEST_CPPTEST_H

extern "C" {
// Get a Java object and modify it
jint Java_cano_ramon_ndktest_MainActivity_getObjectAndAdd (JNIEnv * env , jobject /* this */, jobject obj) ;

// Get two ints and add them returning the result
jint Java_cano_ramon_ndktest_MainActivity_addTwoNumbers (JNIEnv * env , jobject /* this */, jint a, jint b) ;

// Get a Java object and execute its methods
void Java_cano_ramon_ndktest_MainActivity_accessMethods(JNIEnv* env, jobject thisObj, jobject obj);

// Create a native C++ object and return its reference
jlong Java_cano_ramon_ndktest_MainActivity_getNativeObject(JNIEnv* env, jobject thisObj);

// Get the reference to a native C++ object and modify it
void Java_cano_ramon_ndktest_MainActivity_modifyNativeObject(JNIEnv* env, jobject thisObj, jlong addr);

// Get a class and make a Java instance from C++ and return it (!!! No he pogut guardar una referencia als objectes creats al codi C++ !!!)
jobject Java_cano_ramon_ndktest_MainActivity_createJavaInstance(JNIEnv* env, jobject thisObj, jclass cls);

// Get an objects array and modify them
void Java_cano_ramon_ndktest_MainActivity_modifyArray(JNIEnv* env, jobject thisObj, jobjectArray objArray);

}

//std::vector<jobject*> myObjects;
jobjectArray myObjects;

#endif //NDKTEST_CPPTEST_H
